<?php

declare(strict_types = 1);

namespace Drupal\contentful;

use Contentful\Delivery\Client;
use Drupal\Core\Config\ConfigFactory;

/**
 * ContentfulClient service to access all the client resource.
 */
class ContentfulClient {

  /**
   * The configuration factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $configFactory;

  /**
   * ClientFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The ConfigFactory object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get the client service response from contentful.
   *
   * @return object
   *   The client service response
   */
  public function getClientResponse() {
    $config = $this->configFactory->get('contentful.settings');
    $client = new Client($config->get('accessToken'), $config->get('spaceId'));
    return $client;
  }

}
