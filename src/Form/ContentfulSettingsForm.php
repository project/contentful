<?php

declare(strict_types = 1);

namespace Drupal\contentful\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides contentful settings form.
 */
class ContentfulSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contentful_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contentful.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['accessId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access ID'),
      '#default_value' => $this->config('contentful.settings')->get('accessId'),
      '#required' => TRUE,
    ];

    $form['accessToken'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#default_value' => $this->config('contentful.settings')->get('accessToken'),
      '#required' => TRUE,
    ];

    $form['spaceId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Space ID'),
      '#default_value' => $this->config('contentful.settings')->get('spaceId'),
      '#required' => TRUE,
    ];

    $form['enviornmentId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enviornment ID'),
      '#default_value' => $this->config('contentful.settings')
        ->get('enviornmentId'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('contentful.settings')
      ->set('accessId', $form_state->getValue('accessId'))
      ->set('accessToken', $form_state->getValue('accessToken'))
      ->set('spaceId', $form_state->getValue('spaceId'))
      ->set('enviornmentId', $form_state->getValue('enviornmentId'))
      ->save();
  }

}
