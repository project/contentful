<?php

declare(strict_types = 1);

namespace Drupal\contentful;

use Contentful\Delivery\Query;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Contentful\Core\Exception\NotFoundException;

/**
 * ContentfulApi service to access all the Contentful APIs.
 */
class ContentfulApi {
  /**
   * The contentful client object.
   *
   * @var Drupal\contentful\ContentfulClient
   */
  private $contentfulClient;

  /**
   * ContentfulApi constructor.
   *
   * @param \Drupal\contentful\ContentfulClient $contentful_client
   *   The ContentfulClient object.
   */
  public function __construct(ContentfulClient $contentful_client) {
    $this->contentfulClient = $contentful_client;
  }

  /**
   * Get contentfulclient service response.
   *
   * @return object
   *   The contentful client object
   */
  public function getclient() {
    return $this->contentfulClient->getClientResponse();
  }

  /**
   * Get the collection of ContentType objects.
   *
   * @return object[]
   *   Returns a collection of ContentType objects wrapped in a ResourceArray
   *   instance.
   */
  public function getContentTypes() {
    try {
      $contentTypes = $this->getclient()->getContentTypes();
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $contentTypes;
  }

  /**
   * Get a single ContentType object.
   *
   * @param string $contentTypeId
   *   The content type id.
   *
   * @return object
   *   Returns a single ContentType object corresponding to the given ID.
   */
  public function getContentType($contentTypeId) {
    try {
      $contentType = $this->getclient()->getContentType($contentTypeId);

    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $contentType;
  }

  /**
   * Get a collection of Entry objects.
   *
   * @param string $contentTypeId
   *   The content type id.
   * @param array $queryParams
   *   The query parameter.
   *
   * @return object[]
   *   Returns a collection of Entry objects wrapped in a ResourceArray
   *   instance.
   */
  public function getEntries($contentTypeId, array $queryParams = []) {
    try {
      $query = new Query();
      $query->setContentType($contentTypeId);
      foreach ($queryParams as $key => $value) {
        $query->$key($value);
      }
      $entries = $this->getclient()->getEntries($query);
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }

    return $entries;
  }

  /**
   * Get a single Entry object.
   *
   * @param string $entryId
   *   The entry id.
   *
   * @return object
   *   Returns a single Entry object corresponding to the given ID.
   */
  public function getEntry($entryId) {
    try {

      $entry = $this->getclient()->getEntry($entryId);
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $entry;
  }

  /**
   * Get a collection of Asset objects.
   *
   * @return object[]
   *   Returns a collection of Asset objects wrapped in a ResourceArray
   *   instance.
   */
  public function getAssets() {
    try {
      $assets = $this->getclient()->getAssets();
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $assets;
  }

  /**
   * Get a single Asset object.
   *
   * @param string $assetId
   *   The Asset Id.
   *
   * @return object
   *   Returns a single Asset object corresponding to the given ID.
   */
  public function getAsset($assetId) {
    try {
      $asset = $this->getclient()->getAsset($assetId);
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $asset;

  }

  /**
   * Get all tags in the current space and environment.
   *
   * @return array[]
   *   Returns all tags in the current space and environment.
   */
  public function getTags() {
    try {
      $tags = $this->getclient()->getAllTags();
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $tags;
  }

  /**
   * Get a specific tag by its id.
   *
   * @param string $tagId
   *   The tag Id.
   *
   * @return string
   *   Find a specific tag by its id.
   */
  public function getTag($tagId) {
    try {
      $tag = $this->getclient()->getTag($tagId);
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $tag;

  }

  /**
   * Get a signed asset.
   *
   * @param string $assetId
   *   The Asset Id.
   * @param int $expiresIn
   *   The expiration time.
   *
   * @return string
   *   A signed asset
   */
  public function signAsset($assetId, $expiresIn = 300) {
    try {
      $signedAsset = $this->getclient()->getSignedUrl($assetId, $expiresIn);
    }
    catch (NotFoundException $contentfulException) {
      throw new NotFoundHttpException();
    }
    return $signedAsset;
  }

}
